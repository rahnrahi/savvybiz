from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from django.http import JsonResponse                                                                                 
from core.models import Company, Address, Timezone, User
from core_api.serializers import CompanySerializer, AddressSerializer, TimezoneSerializer


def obtain_auth_token(request):
    #  print(request.POST.get('user'))
     return JsonResponse({"token":"sdfdsfsd"})                                                

class CompanyViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Return the given Company.

    list:
    Return a list of all the existing Companies.

    create:
    Create a new Company instance.

    update:
    Update existing Company instance.

    partial_update:
    Partial update existing Company instance.

    destroy:
    Destroy existing Company instance.
    """

    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class AddressViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Return the given Address.

    list:
    Return a list of all the existing Companies.

    create:
    Create a new Address instance.

    update:
    Update existing Company instance.

    partial_update:
    Partial update existing Company instance.

    destroy:
    Destroy existing Company instance.
    """

    def get_queryset(self):
        """
        This view should return a list of all the Profiles for
        the user as determined by currently logged in user.
        """
        queryset = Address.objects.all()
        queryset.filter(enabled=True)
        if not ((self.request.user.is_superuser or self.request.user.is_staff) and not hasattr(self.request.user, 'company')):
            queryset.filter(company=self.request.user.company)
        return queryset

    serializer_class = AddressSerializer


class CountryViewSet(viewsets.ModelViewSet):

    queryset = Timezone.objects.all()

    def get_queryset(self):
        """
        This view should return a list of all the Timezones except those disabled.
        """
        return Timezone.objects.filter(enabled=True)

    serializer_class = TimezoneSerializer


class StateViewSet(viewsets.ModelViewSet):

    queryset = Timezone.objects.all()

    def get_queryset(self):
        """
        This view should return a list of all the Timezones except those disabled.
        """
        return Timezone.objects.filter(enabled=True)

    serializer_class = TimezoneSerializer


class TimezoneViewSet(viewsets.ModelViewSet):

    queryset = Timezone.objects.all()

    def get_queryset(self):
        """
        This view should return a list of all the Timezones except those disabled.
        """
        return Timezone.objects.filter(enabled=True)

    serializer_class = TimezoneSerializer
