from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from django.http import JsonResponse
from core.models import Company, Address, Timezone
from core_api.serializers import CompanySerializer, AddressSerializer, TimezoneSerializer



# def obtain_auth_token(request):
#     print(request.GET.get('user'))
#     user = authenticate(username='dev', password='testtest')
#     if user is not None:
#       token = Token.objects.get_or_create(user=user)
#       obj = {
#         "token": token.key
#       }
#       return JsonResponse({"token": obj})
#     else:
#        return JsonResponse({"token": 0})


class CompanyViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Return the given Company.

    list:
    Return a list of all the existing Companies.

    create:
    Create a new Company instance.

    update:
    Update existing Company instance.

    partial_update:
    Partial update existing Company instance.

    destroy:
    Destroy existing Company instance.
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class AddressViewSet(viewsets.ModelViewSet):
    """
    retrieve:
    Return the given Address.

    list:
    Return a list of all the existing Companies.

    create:
    Create a new Address instance.

    update:
    Update existing Company instance.

    partial_update:
    Partial update existing Company instance.

    destroy:
    Destroy existing Company instance.
    """
    authentication_classes = (TokenAuthentication)

    def get_queryset(self):
        """
        This view should return a list of all the Profiles for
        the user as determined by currently logged in user.
        """
        queryset = Address.objects.all()
        queryset.filter(enabled=True)
        if not ((self.request.user.is_superuser or self.request.user.is_staff) and not hasattr(self.request.user, 'company')):
            queryset.filter(company=self.request.user.company)
        return queryset

    serializer_class = AddressSerializer


class CountryViewSet(viewsets.ModelViewSet):

    queryset = Timezone.objects.all()

    def get_queryset(self):
        """
        This view should return a list of all the Timezones except those disabled.
        """
        return Timezone.objects.filter(enabled=True)

    serializer_class = TimezoneSerializer


class StateViewSet(viewsets.ModelViewSet):

    queryset = Timezone.objects.all()

    def get_queryset(self):
        """
        This view should return a list of all the Timezones except those disabled.
        """
        return Timezone.objects.filter(enabled=True)

    serializer_class = TimezoneSerializer


class TimezoneViewSet(viewsets.ModelViewSet):

    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    # def get(self, request, format=None):
    #     return Response("Hello {0}!".format(request.user))

    queryset = Timezone.objects.all()

    def get_queryset(self):
        """
        This view should return a list of all the Timezones except those disabled.
        """
        return Timezone.objects.filter(enabled=True)

    serializer_class = TimezoneSerializer
